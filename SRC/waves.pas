unit waves;

interface

type
  TWaveHeader = record
    idRiff: array[0..3] of char;
    RiffLen: longint;
    idWave: array[0..3] of char;
  end;
  TFmtHeader = record
    idFmt: array[0..3] of char;
    InfoLen: longint;
    WaveType: smallint;
    Ch: smallint;
    Freq: longint;
    BytesPerSec: longint;
    align: smallint;
    Bits: smallint;
  end;
  TDataHeader = record
    idData: array[0..3] of char;
    DataLen: longint;
  end;

implementation

end.