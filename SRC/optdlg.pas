Unit optdlg;

interface

Uses Dialogs;

type
  ToptData = record
    _LoadAddr:String[6];
    _StartAddr:String[6];
    _WriteWav:Boolean;
  End;

type
  POptDialog = ^TOptDialog;
  TOptDialog = object (TDialog)
     LoadAddrLine:PInputLine;
     StartAddrLine:PInputLine;
     Constructor Init;
  End;

implementation

Uses Objects, Views;

Constructor TOptDialog.Init;
Var
  R:TRect;
Begin

  R.Assign(0, 0, 34, 12);
  TDialog.Init( R, 'Options');

  R.Assign(3, 4, 30, 5);
  options:= options or ofCentered;

  R.Assign( 17, 2, 25, 3 );
  LoadAddrLine:= new(PInputLine, Init( R, 6 ));
  insert( LoadAddrLine );

  R.Assign( 17, 4, 25, 5 );
  StartAddrLine:= new(PInputLine, Init( R, 6 ));
  insert( StartAddrLine );

  R.Assign(2, 2, 15, 3);
  insert(new(PLabel, Init(r, 'LoadAddr = ', Nil)));

  R.Assign(2, 4, 15, 5);
  insert(new(PLabel, Init(r, 'StartAddr = ',  Nil)));

  R.Assign(2, 6, 25, 7);
  Insert(New(PCheckBoxes, Init(R,
    NewSItem('~W~rite wave',nil))));

  R.Assign(6, 9, 16, 11);
  insert(new(PButton, Init(r, 'O~K~', cmOK, bfDefault)));

  R.Assign(18, 9, 28, 11);
  insert(new(PButton, Init(r, 'Cancel', cmCancel, bfNormal)));

  selectNext(false);

end;

end.
